// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('broScore', [
  'ionic',
  'broScore.controllers',
  'broScore.factories',
  'broScore.filters',
  'ngMessages',
  'ngCordova',
  'angularMoment',
  'parse-angular',
  'parse-angular.enhance',
  'angular-md5',
  'monospaced.qrcode',
  'ngTagsInput'
])

  .run(function ($ionicPlatform, $rootScope, $state) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      // Detect the start of a state change
      $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
        if(toState.module === 'private' && !Parse.User.current()) {
          e.preventDefault();
          $state.go('login');
        }
      });

    });

    // Initialise Parse
    Parse.initialize("K9HW7CXBhFl9gPfFjfnBf6PP0ArbWNlfQvIsyDYD", "vU3PXLGEetBKGKWYxYJWBmKTfPjqtBmGXi34EKMj");
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('login', {
        url: "/login",
        module: 'public',
        templateUrl: 'templates/pages/auth/login.html',
        controller: 'AuthCtrl',
        onEnter: function($state) {
          if(Parse.User.current()) {
            $state.go('app.brosquad.dashboard');
          }
        }
      })
      .state('register', {
        url: "/register",
        module: 'public',
        templateUrl: 'templates/pages/auth/register.html',
        controller: 'AuthCtrl',
        onEnter: function($state) {
          if(Parse.User.current()) {
            $state.go('app.brosquad.dashboard');
          }
        }
      })
      .state('app', {
        url: "/app",
        module: 'private',
        abstract: true,
        templateUrl: "templates/pages/menu.html",
        controller: "AppCtrl",
        resolve: {
          user: function (Auth) {
            return Auth.user;
          }
        }
      })
      .state('app.brosquad', {
        url: "/brosquad",
        module: 'private',
        abstract: true,
        views: {
          'menuContent': {
            templateUrl: "templates/pages/brosquad/index.html"
          }
        }
      })
      .state('app.brosquad.dashboard', {
        url: '/dashboard',
        module: 'private',
        views: {
          'tab-brosquad-dashboard': {
            templateUrl: "templates/pages/brosquad/dashboard.html",
            controller: "DashBroSquad"
          }
        }
      })
      .state('app.brosquad.join', {
        url: '/join',
        module: 'private',
        views: {
          'tab-brosquad-join': {
            templateUrl: "templates/pages/brosquad/join.html",
            controller: "JoinBroSquad"
          }
        }
      })
      .state('app.brosquad.create', {
        url: '/create',
        module: 'private',
        views: {
          'tab-brosquad-create': {
            templateUrl: "templates/pages/brosquad/create.html",
            controller: "CreateBroSquad"
          }
        }
      })
      .state('app.brosquad.squad', {
        url: '/:id',
        params: {
          id: null
        },
        module: 'private',
        abstract: true,
        views: {
          'menuContent@app': {
            templateUrl: "templates/pages/brosquad/squad.html"
          }
        }
      })
      .state('app.brosquad.squad.newsfeed', {
        url: '/newsfeed',
        module: 'private',
        views: {
          'tab-squad-newsfeed': {
            templateUrl: "templates/pages/brosquad/squad/newsfeed.html",
            controller: "SquadNewsFeed"
          }
        }
      })
      .state('app.brosquad.squad.vote', {
        url: '/start',
        module: 'private',
        views: {
          'tab-squad-vote': {
            templateUrl: "templates/pages/brosquad/squad/callvote.html",
            controller: "StartVote"
          }
        }
      })
      .state('app.profile', {
        url: "/profile",
        module: 'private',
        views: {
          'menuContent': {
            templateUrl: "templates/pages/user/profile.html",
            controller: 'EditProfileCtrl'
          }
        }
      })

    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
  })

  .config(function( $ionicConfigProvider) {
    $ionicConfigProvider.navBar.alignTitle('center');
  });
