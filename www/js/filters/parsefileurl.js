angular.module('broScore.filters.parsefileurl', [])
  .filter('parseFileUrl', function() {
    return function(input, scope) {
      if (input) {
        var formattedUrl = input.replace(/^http:\/\//, '');
        return 'https://s3.amazonaws.com/' + formattedUrl;
      } else {
        return '';
      }
    }
  });
