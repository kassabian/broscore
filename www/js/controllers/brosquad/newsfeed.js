angular.module('broScore.controllers.newsfeed', [])
  .controller('SquadNewsFeed', ['$scope', '$ionicPlatform', '$ionicLoading', '$ionicPopup', '$state', 'Auth', 'BroSquad', 'BroVote', function($scope, $ionicPlatform, $ionicLoading, $ionicPopup, $state, Auth, BroSquad, BroVote) {
    $scope.user = Auth.user;
    $scope.squad = {};
    $scope.squad.name = 'YMCMB';

    console.log($state.params.id);

    BroVote.getSquadMembers($state.params.id).then(function(data) {
      console.log(data, 123);
    }, function(err) {
      console.log(err, 9);
    });

    $scope.backButton = function() {
      $state.go('app.brosquad.dashboard');
    };
  }]);
