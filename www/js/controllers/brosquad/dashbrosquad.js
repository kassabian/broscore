angular.module('broScore.controllers.dashbrosquad', [])
  .controller('DashBroSquad', ['$scope', '$ionicPlatform', '$ionicLoading', '$ionicPopup', 'BroSquad', function($scope, $ionicPlatform, $ionicLoading, $ionicPopup, BroSquad) {
    $scope.broSquads = [];
    $scope.loadBroSquads = loadBroSquads;
    $scope.$on( "$ionicView.enter", function() {
      loadBroSquads(null);
    });


    // loadBroSquads()
    // @param caller string
    function loadBroSquads(caller) {
      if(caller != 'refresh') { $ionicLoading.show({template: 'Fetching Your BroSquads...'}); }

      BroSquad.list().then(function (data) {
        (caller != 'refresh') ? $ionicLoading.hide() : $scope.$broadcast('scroll.refreshComplete');
        console.log(data);
        $scope.broSquads = data;
      }, function (err) {
        if(caller != 'refresh') { $ionicLoading.hide(); }
        $ionicPopup.alert({
          title: 'Error fetching your BroSquads',
          subtitle: err.message
        });
      });
    }

    $scope.leaveBroSquad = function(index, squad) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Leave BroSquad',
        template: "Are you sure you don't wanna bro-out with your squad anymore?"
      });
      confirmPopup.then(function(response) {
        if(response) {
          BroSquad.leave(squad).then(function () {
            $scope.broSquads.splice(index, 1);
          }, function() {
            $ionicPopup.alert({
              title: 'An error has occurred while trying to bail out on your bros!',
              subtitle: err.message
            });
          });
        }
      });
    }; // ***END leaveBroSquad()

  }]);
