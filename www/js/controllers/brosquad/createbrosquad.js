angular.module('broScore.controllers.createbrosquad', [])
  .controller('CreateBroSquad', ['$scope', '$ionicPlatform', '$cordovaCamera', '$ionicLoading', '$ionicPopup', 'BroSquad', function($scope, $ionicPlatform, $cordovaCamera, $ionicLoading, $ionicPopup, BroSquad) {
    $scope.formData = {
      title: '',
      emails: [],
      picture: null
    };

    $scope.createBroSquad = function(form) {
      if(form.$valid) {
        $ionicLoading.show({
          template: 'Creating BroSquad...'
        });
        // Iterate through email addresses
        delete $scope.formData.emails;

        // Create BroSquad
        BroSquad.create($scope.formData).then(function(data) {
          console.log(data, 'BroSquad Created');
        }, function(err) {
          $ionicPopup.alert({
            title: 'Error Creating BroSquad!',
            subtitle: err.message
          });
        }).finally(function() {
          $ionicLoading.hide();
        });
      }
    };

    $scope.addPicture = function () {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY, // CAMERA
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 360,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $ionicPlatform.ready(function () {
        $cordovaCamera.getPicture(options).then(function(imageData) {
          $scope.formData.picture = imageData;
        }, function(err) {
          $ionicPopup.alert({
            title: 'Error Taking Photo!',
            subTitle: 'We had an issue trying to take that picture, please try again'
          });
        });
      }); // ***END $ionicPlatform.ready()
    }; // ***END addPicture()

  }]);
