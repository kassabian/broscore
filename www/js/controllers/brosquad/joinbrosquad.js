angular.module('broScore.controllers.joinbrosquad', [])
  .controller('JoinBroSquad', ['$scope', '$ionicPlatform', '$ionicPopup', '$ionicLoading', '$cordovaBarcodeScanner', '$state', 'BroSquad', function($scope, $ionicPlatform, $ionicPopup, $ionicLoading, $cordovaBarcodeScanner, $state, BroSquad) {
    $scope.formData = {};

    $scope.scan = function() {
      $ionicPlatform.ready(function() {
        $cordovaBarcodeScanner.scan().then(function(barcodeData) {
            $scope.formData.inviteCode = barcodeData.text;
          }, function(error) {
            $ionicPopup.alert({
              title: "Error!",
              subTitle: "Oops! Looks like the barcode scanner didn't work. Please try again."
            });
          });
      });
    };

    $scope.join = function() {
      $ionicLoading.show({template: 'Joining the Squad...'});

      BroSquad.join($scope.formData).then(function() {
        $scope.formData.inviteCode = null;
        $state.go('app.brosquad.dashboard');
      }, function(err) {
        $ionicPopup.alert({
          title: "Error!",
          subTitle: "Oops! Looks like you weren't able to join the squad. Please try again."
        });
      }).finally(function() {
        $ionicLoading.hide();
      })
    };

  }]);
