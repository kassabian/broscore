angular.module('broScore.controllers.authctrl', [])

.controller('AuthCtrl', ['$rootScope', '$scope', '$state', 'Auth', '$cordovaOauth', function($rootScope, $scope, $state, Auth, $cordovaOauth) {
    $scope.formData = {};

    $scope.loginUser = function(form) {
      if(form.$valid) {
        Auth.login($scope.formData).then(function(user) {
          console.log(user);
          $rootScope.$emit('successfulLogin:emit', {
            user: user
          });
          $state.go('app.brosquad.dashboard');
        });
      }
    };

    $scope.registerUser = function(form) {
      if(form.$valid) {
        Auth.register($scope.formData).then(function (user) {
          $state.go('app.brosquad.dashboard');
        });
      }
    };
}]);
