angular.module('broScore.controllers.appctrl', [])
  .controller('AppCtrl', ['$rootScope', '$scope', '$state', 'Auth', 'user', function($rootScope, $scope, $state, Auth, user) {
    $scope.user = user;

    $scope.logout = function() {
      Parse.User.logOut();
      $state.go('login');
    };
  }]);
