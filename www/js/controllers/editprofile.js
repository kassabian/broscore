angular.module('broScore.controllers.editprofilectrl', [])
  .controller('EditProfileCtrl', ['$scope', 'Auth', '$cordovaToast', function ($scope, Auth, $cordovaToast) {
    $scope.user = Auth.user;

    $scope.formData = {
      email: $scope.user.attributes.email,
      firstName: $scope.user.attributes.firstName,
      lastName: $scope.user.attributes.lastName
    };

    $scope.updateProfile = function(form) {
      console.log($scope.user);
      if(form.$valid) {
        Auth.updateProfile($scope.formData).then(function (updatedUser) {
          console.log(updatedUser, 8);

          $scope.formData = {
            email: updatedUser.attributes.email,
            firstName: updatedUser.attributes.firstName,
            lastName: updatedUser.attributes.lastName
          };
          $cordovaToast.showShortBottom('Profile Updated!');
        });
      }
    };

  }]);
