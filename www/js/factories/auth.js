angular.module('broScore.factories.auth', [])

.factory('Auth', ['$q', '$ionicPopup', function($q, $ionicPopup) {
    var obj = {
      user: Parse.User.current(),

      login: function(formData) {
        var deferred = $q.defer();

        Parse.User.logIn(formData.email, formData.password, {
          success: function(user) {
            obj.user = user;
            deferred.resolve(user);
          },
          error: function(user, error) {
            $ionicPopup.alert({
              title: 'Sign in error!',
              subtitle: error.message
            });
            deferred.reject(error);
          }
        });

        return deferred.promise;
      },

      register: function(formData) {
        var deferred = $q.defer();

        var user = new Parse.User();
        user.set('email', formData.email);
        user.set('username', formData.email);
        user.set('firstName', formData.firstName);
        user.set('lastName', formData.lastName);
        user.set('password', formData.password);

        user.signUp(null, {
          success: function(user) {
            console.log('Account created');
            obj.user = user;

            deferred.resolve(obj.user);
          },
          error: function(user, error) {
            $ionicPopup.alert({
              title: 'Registration Error!',
              subtitle: error.message
            });

            deferred.reject(error);
          }
        });

        return deferred.promise;
      },

      updateProfile: function(formData) {
        var deferred = $q.defer();

        var User = Parse.Object.extend("User");
        var query = new Parse.Query(User);
        query.equalTo("email", formData.email);

        if(this.user && (this.user.attributes.email != formData.email)) {
          // Check to see if the updated email provided already exists, if so throw an error
          query.first({
            success: function (results) {
              if (results.length > 0) {
                $ionicPopup.alert({
                  title: 'Email Already Exists!',
                  subtitle: 'This email address is already in use. Your profile has not been updated.'
                });

                deferred.reject();
              }
            }
          });
        } // ***END if{}

        query.first({
          success: function(user) {
            user.set('email', formData.email);
            user.set('username', formData.email);
            user.set("firstName", formData.firstName);
            user.set("lastName", formData.lastName);
            user.save(null, {
              success: function(updatedUser) {
                obj.user = updatedUser;

                deferred.resolve(obj.user);
              }
            });
          },
          error: function(user, error) {
            $ionicPopup.alert({
              title: 'Update was NOT successful. Please try again.',
              subtitle: error.message
            });
            deferred.reject(error);
          }
        });
        return deferred.promise;
      }
    };

    return obj;
}]);
