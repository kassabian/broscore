angular.module('broScore.factories.brosquad', [])
  .factory('BroSquad', ['$q', 'Auth', function($q, Auth) {
    var obj = {
      // Create a BroSquad and assign the creating user the President title
      create: function(formData) {
        var self = this;
        var deferred = $q.defer();

        var BroSquad = Parse.Object.extend("BroSquad");
        var squad = new BroSquad();
        squad.set("owner", Auth.user);
        squad.set("title", formData.title);
        squad.set("picture", (formData.picture ? new Parse.File("photo.jpg", {base64: formData.picture}) : null));
        squad.set("created", new Date());

        squad.save({}).then(function (newSquad) {
          // Squad has been created, now add the squad president into BroSquadMembers
          self.newSquad = newSquad;
          var user = Auth.user;
          var relation = user.relation("BroSquadMembers");
          relation.add(newSquad);

          return user.save();
        })
        .then(function(data) {
          console.log(self.newSquad, 123);
          deferred.resolve(self.newSquad);
        }, function(err) {
          deferred.reject(err);
        });

        return deferred.promise;
      }, // ***END create()

      // List all BroSquads that the current user is a part of
      list: function() {
        var deferred = $q.defer();

        var user = Auth.user;
        var relation = user.relation("BroSquadMembers");
        relation.query().find().then(function(data) {
          deferred.resolve(data);
        }, function(err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      // Join a squad based on invite code
      join: function(formData) {
        var deferred = $q.defer();
        var BroSquad = Parse.Object.extend("BroSquad");
        var query = new Parse.Query(BroSquad);
        query.equalTo("objectId", formData.inviteCode);

        query.first().then(function (squad) {
          var user = Auth.user;
          var relation = user.relation("BroSquadMembers");
          relation.add(squad);

          return user.save();
        }).then(function(data) {
          deferred.resolve(data);
        }, function(err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      // Leave a bro squad
      leave: function(squad) {
        var deferred = $q.defer();

        var user = Auth.user;
        var relation = user.relation("BroSquadMembers");
        relation.remove(squad);
        user.save().then(function(data) {
          deferred.resolve(data);
        }, function(err) {
          deferred.reject(err);
        });

        return deferred.promise;
      }

    };

    return obj;
  }]);
