angular.module('broScore.factories.brovote', [])
  .factory('BroVote', ['$q', 'Auth', function($q, Auth) {
    var obj = {
      currentSquad: null,

      getSquadMembers: function(squadId) {
        var deferred = $q.defer();

        var BroSquad = Parse.Object.extend("BroSquad");
        var query = new Parse.Query(BroSquad);
        query.equalTo("objectId", squadId);
        query.include(Parse.User);

        query.first().then(function (squad) {
          var r = squad.relation(Parse.User);
          var q = r.query();

          return q.find();
        }).then(function(results) {
          deferred.resolve(results);
        }, function(err) {
          deferred.reject(err);
        });
        return deferred.promise;
      }
    };

    return obj;
  }]);
