angular.module('broScore.controllers', [
  'broScore.controllers.appctrl',
  'broScore.controllers.authctrl',
  'broScore.controllers.createbrosquad',
  'broScore.controllers.dashbrosquad',
  'broScore.controllers.editprofilectrl',
  'broScore.controllers.joinbrosquad',
  'broScore.controllers.newsfeed',
  'broScore.controllers.startvote'
]);
